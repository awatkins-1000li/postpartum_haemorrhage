class ReworkCauseOfBleedInPPHEpisodes < ActiveRecord::Migration[5.0]
  def up

    add_column :pph_episodes, :cause_of_bleed_uterine_atrophy, :boolean
    add_column :pph_episodes, :cause_of_bleed_surgical, :boolean
    add_column :pph_episodes, :cause_of_bleed_genital_tract_trauma, :boolean
    add_column :pph_episodes, :cause_of_bleed_uterine_rupture, :boolean
    add_column :pph_episodes, :cause_of_bleed_abruption, :boolean
    add_column :pph_episodes, :cause_of_bleed_placenta_praevia, :boolean
    add_column :pph_episodes, :cause_of_bleed_retained_products, :boolean
    add_column :pph_episodes, :cause_of_bleed_placenta_ac_increta, :boolean
    add_column :pph_episodes, :cause_of_bleed_amniotic_fluid_embolism, :boolean
    add_column :pph_episodes, :cause_of_bleed_extragenital_bleed, :boolean

    remove_column :pph_episodes, :cause_of_bleed_primary
    remove_column :pph_episodes, :cause_of_bleed_secondary

  end

  def down

    remove_column :pph_episodes, :cause_of_bleed_uterine_atrophy
    remove_column :pph_episodes, :cause_of_bleed_surgical
    remove_column :pph_episodes, :cause_of_bleed_genital_tract_trauma
    remove_column :pph_episodes, :cause_of_bleed_uterine_rupture
    remove_column :pph_episodes, :cause_of_bleed_abruption
    remove_column :pph_episodes, :cause_of_bleed_placenta_praevia
    remove_column :pph_episodes, :cause_of_bleed_retained_products
    remove_column :pph_episodes, :cause_of_bleed_placenta_ac_increta
    remove_column :pph_episodes, :cause_of_bleed_amniotic_fluid_embolism
    remove_column :pph_episodes, :cause_of_bleed_extragenital_bleed

    add_column :pph_episodes, :cause_of_bleed_primary, :string
    add_column :pph_episodes, :cause_of_bleed_secondary, :string

  end
end
