class AddKerberosPrincipalToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :kerberos_principal, :string
  end
end
