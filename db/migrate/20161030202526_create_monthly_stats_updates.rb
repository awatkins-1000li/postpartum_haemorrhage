class CreateMonthlyStatsUpdates < ActiveRecord::Migration[5.0]
  def change
    create_table :monthly_stats_updates do |t|
      t.date :month

      t.integer :obstetric_led_maternities
      t.integer :obstetric_led_deliveries

      t.integer :midwife_led_maternities
      t.integer :midwife_led_deliveries

      t.timestamps
    end
  end
end
