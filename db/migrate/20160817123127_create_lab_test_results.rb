class CreateLabTestResults < ActiveRecord::Migration[5.0]
  def change
    create_table :lab_test_results do |t|

      t.datetime :results_available_at

      t.integer :haemoglobin_in_grams_per_litre
      t.integer :platelets_in_thousand_millions_per_litre
      t.integer :activated_partial_thromboplastin_time_in_seconds
      t.integer :prothrombin_time_in_seconds
      t.integer :fibrinogen_in_grams_per_litre

      t.references :pph_episode, foreign_key: true

      t.timestamps
    end
  end
end
