class AddSurgicalInterventionsToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :haemostatic_uterine_suture_performed, :string
    add_column :pph_episodes, :bakri_balloon_performed, :string
    add_column :pph_episodes, :internal_iliac_artery_ligation_performed, :string
  end
end
