class ChangeLactateToDecimal < ActiveRecord::Migration[5.0]
  def up
    change_column :blood_gas_results, :lactate_in_millimoles_per_litre, :decimal, precision:4, scale:1
  end

  def down
    change_column :blood_gas_results, :lactate_in_millimoles_per_litre, :integer
  end
end
