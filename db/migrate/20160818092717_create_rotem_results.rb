class CreateROTEMResults < ActiveRecord::Migration[5.0]
  def change
    create_table :rotem_results do |t|

      t.datetime :available_at

      t.integer :fibtem_a5_in_millimetres
      t.integer :extem_a5_in_millimetres
      t.integer :intem_a5_in_millimetres

      t.integer :extem_ct_in_seconds
      t.integer :intem_ct_in_seconds

      t.references :pph_episode, foreign_key: true

      t.timestamps
    end
  end
end
