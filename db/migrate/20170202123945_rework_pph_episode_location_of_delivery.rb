class ReworkPPHEpisodeLocationOfDelivery < ActiveRecord::Migration[5.0]
  def up
    PPHEpisode.where(location_of_delivery:'Midwife-led Unit').update_all(location_of_delivery:'Alongside Midwifery-led Unit')
    PPHEpisode.where(location_of_delivery:'Community').update_all(location_of_delivery:'Home')
  end

  def down
    PPHEpisode.where(location_of_delivery:'Alongside Midwifery-led Unit').update_all(location_of_delivery:'Midwife-led Unit')
    PPHEpisode.where(location_of_delivery:'Home').update_all(location_of_delivery:'Community')
  end
end
