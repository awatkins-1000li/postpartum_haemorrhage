class AddAnyLevel2CareDeliveredOutsideOfDeliveryUnitToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :woman_received_level_2_hdu_care_outside_of_delivery_unit, :string
  end
end
