class AddNoROTEMsCheckboxToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    change_table :pph_episodes do |t|
      t.boolean :confirm_no_rotems
    end
  end
end
