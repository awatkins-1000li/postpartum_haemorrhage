class MigrateBloodGasResultsToBloodResults < ActiveRecord::Migration[5.0]
  def up
    BloodGasResult.all.each do |bgr|
      br = BloodResult.create(
          result_at: bgr.test_at,
          haemoglobin_in_grams_per_litre: bgr.haemoglobin_in_grams_per_litre,
          lactate_in_millimoles_per_litre: bgr.lactate_in_millimoles_per_litre,
          pph_episode_id: bgr.pph_episode_id,
          origin:"blood_gas_result##{bgr.id}"
        )

      bgr.destroy if br.errors.none?
    end
  end

  def down
    BloodResult.where('origin LIKE "blood_gas_result%"').all.each do |br|

      bgr = BloodGasResult.create(
          test_at: br.result_at,
          haemoglobin_in_grams_per_litre: br.haemoglobin_in_grams_per_litre,
          lactate_in_millimoles_per_litre: br.lactate_in_millimoles_per_litre,
          pph_episode_id: br.pph_episode_id
        )

      br.destroy if bgr.errors.none?
    end
  end
end
