class AddRiskAssessmentCompletedToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :risk_assessment_completed, :string
  end
end
