class ChangePrecisionOfColumnsInLabTestResults < ActiveRecord::Migration[5.0]
  def up
    change_column :lab_test_results, :activated_partial_thromboplastin_time_in_seconds, :decimal, precision:6, scale:2
    change_column :lab_test_results, :prothrombin_time_in_seconds, :decimal, precision:6, scale:2
    change_column :lab_test_results, :fibrinogen_in_grams_per_litre, :decimal, precision:6, scale:2
  end

  def down
    change_column :lab_test_results, :activated_partial_thromboplastin_time_in_seconds, :integer
    change_column :lab_test_results, :prothrombin_time_in_seconds, :integer
    change_column :lab_test_results, :fibrinogen_in_grams_per_litre, :integer
  end
end
