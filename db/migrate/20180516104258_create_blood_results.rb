class CreateBloodResults < ActiveRecord::Migration[5.0]
  def change
    create_table :blood_results do |t|

      t.datetime "result_at"

      t.integer "haemoglobin_in_grams_per_litre"
      t.decimal "lactate_in_millimoles_per_litre", precision: 4, scale: 1

      t.integer "platelets_in_thousand_millions_per_litre"
      t.decimal "activated_partial_thromboplastin_time_in_seconds", precision: 6, scale: 2
      t.decimal "prothrombin_time_in_seconds",                      precision: 6, scale: 2
      t.decimal "fibrinogen_in_grams_per_litre",                    precision: 6, scale: 2

      t.string "origin"

      t.references :pph_episode, foreign_key: true

      t.timestamps

    end
  end
end
