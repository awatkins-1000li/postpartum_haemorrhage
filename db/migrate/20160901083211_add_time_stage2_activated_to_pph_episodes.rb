class AddTimeStage2ActivatedToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :stage_2_activated_or_litre_lost_at, :datetime
  end
end
