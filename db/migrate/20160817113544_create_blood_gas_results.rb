class CreateBloodGasResults < ActiveRecord::Migration[5.0]
  def change
    create_table :blood_gas_results do |t|
      t.datetime :test_at
      t.integer :haemoglobin_in_grams_per_litre
      t.integer :lactate_in_millimoles

      t.timestamps
    end
  end
end
