class AddMaternityUnitToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :maternity_unit, foreign_key: true
  end
end
