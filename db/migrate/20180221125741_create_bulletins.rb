class CreateBulletins < ActiveRecord::Migration[5.0]
  def change
    create_table :bulletins do |t|
      t.text :markdown_text
      t.datetime :post_at

      t.timestamps
    end
  end
end
