class AddLocationOfDeliveryToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :location_of_delivery, :string
  end
end
