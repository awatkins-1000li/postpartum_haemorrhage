class AddLocalIdentifierToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :local_identifier, :string
  end
end
