class CreateMaternityUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :maternity_units do |t|
      t.string :name
      t.timestamps
    end

    add_reference :pph_episodes, :maternity_unit, foreign_key: true
    
  end
end
