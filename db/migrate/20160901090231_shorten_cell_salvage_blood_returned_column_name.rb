class ShortenCellSalvageBloodReturnedColumnName < ActiveRecord::Migration[5.0]
  def change
    rename_column :pph_episodes, :cell_salvage_blood_returned_to_patient_as_tranfusion_in_millilitres, :cell_salvage_blood_returned_in_millilitres
  end
end
