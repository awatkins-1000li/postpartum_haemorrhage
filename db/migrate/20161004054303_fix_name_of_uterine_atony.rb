class FixNameOfUterineAtony < ActiveRecord::Migration[5.0]
  def change
    rename_column :pph_episodes, :cause_of_bleed_uterine_atrophy, :cause_of_bleed_uterine_atony
  end
end
