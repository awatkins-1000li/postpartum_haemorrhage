class ReworkMonthlyStatsUpdates < ActiveRecord::Migration[5.0]
  def change
    rename_column :monthly_stats_updates, :midwife_led_maternities, :alongside_midwifery_led_maternities 
    rename_column :monthly_stats_updates, :midwife_led_deliveries, :alongside_midwifery_led_deliveries

    add_column :monthly_stats_updates, :freestanding_midwifery_led_maternities, :integer
    add_column :monthly_stats_updates, :freestanding_midwifery_led_deliveries, :integer

    add_column :monthly_stats_updates, :home_maternities, :integer
    add_column :monthly_stats_updates, :home_deliveries, :integer
  end
end
