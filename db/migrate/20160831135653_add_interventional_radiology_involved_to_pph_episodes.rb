class AddInterventionalRadiologyInvolvedToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :interventional_radiology_involved, :string
  end
end
