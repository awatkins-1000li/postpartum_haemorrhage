class MigrateLabTestResultsToBloodResults < ActiveRecord::Migration[5.0]
  def up
    LabTestResult.all.each do |ltr|
      br = BloodResult.create(
          result_at: ltr.results_available_at,
          haemoglobin_in_grams_per_litre: ltr.haemoglobin_in_grams_per_litre,
          platelets_in_thousand_millions_per_litre: ltr.platelets_in_thousand_millions_per_litre,
          activated_partial_thromboplastin_time_in_seconds: ltr.activated_partial_thromboplastin_time_in_seconds,
          prothrombin_time_in_seconds: ltr.prothrombin_time_in_seconds,
          fibrinogen_in_grams_per_litre: ltr.fibrinogen_in_grams_per_litre,
          pph_episode_id: ltr.pph_episode_id,
          origin:"lab_test_result##{ltr.id}"
        )

      ltr.destroy if br.errors.none?
    end
  end

  def down
    BloodResult.where('origin LIKE "lab_test_result%"').all.each do |br|

      ltr = LabTestResult.create(
          results_available_at: br.result_at,
          haemoglobin_in_grams_per_litre: br.haemoglobin_in_grams_per_litre,
          lactate_in_millimoles_per_litre: br.lactate_in_millimoles_per_litre,
          pph_episode_id: br.pph_episode_id
        )

      br.destroy if ltr.errors.none?
    end
  end
end