class FixCriticalCareFieldNames < ActiveRecord::Migration[5.0]
  def change
    rename_column :pph_episodes, :woman_required_level_2_hdu_care, :woman_received_level_2_hdu_care
    rename_column :pph_episodes, :woman_transferred_to_level_3_icu_care, :woman_received_level_3_icu_care
  end
end
