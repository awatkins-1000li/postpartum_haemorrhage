class AddFourStagePaperworkUsedToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :four_stage_paperwork_used, :string
  end
end
