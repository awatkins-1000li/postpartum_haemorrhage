class AddPPHEpisodeToBloodGasResults < ActiveRecord::Migration[5.0]
  def change
    add_reference :blood_gas_results, :pph_episode, foreign_key: true
  end
end
