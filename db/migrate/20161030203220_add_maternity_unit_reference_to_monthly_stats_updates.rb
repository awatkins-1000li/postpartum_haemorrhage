class AddMaternityUnitReferenceToMonthlyStatsUpdates < ActiveRecord::Migration[5.0]
  def change
    add_reference :monthly_stats_updates, :maternity_unit, foreign_key: true
  end
end
