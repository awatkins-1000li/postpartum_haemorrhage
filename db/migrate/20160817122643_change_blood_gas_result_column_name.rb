class ChangeBloodGasResultColumnName < ActiveRecord::Migration[5.0]
  def change
    rename_column :blood_gas_results, :lactate_in_millimoles, :lactate_in_millimoles_per_litre
  end
end
