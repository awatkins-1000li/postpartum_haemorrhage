class SplitUterineRuptureOrInversion < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :cause_of_bleed_uterine_inversion, :boolean
  end
end
