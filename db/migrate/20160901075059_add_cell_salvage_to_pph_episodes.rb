class AddCellSalvageToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_column :pph_episodes, :cell_salvage_used, :string
    add_column :pph_episodes, :cell_salvage_blood_returned_to_patient_as_tranfusion_in_millilitres, :integer
  end
end
