class AddTitleToBulletins < ActiveRecord::Migration[5.0]
  def change
    change_table :bulletins do |t|
      t.string :title
    end    
  end
end
