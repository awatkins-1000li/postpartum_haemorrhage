class CreatePPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    create_table :pph_episodes do |t|
      t.datetime :delivered_at, comment: 'End of all deliveries'
      t.datetime :discharged_at

      t.integer :number_of_deliveries
      t.string :type_of_delivery, comment: 'If multiple deliveries, then the most invasive should be chosen.'

      t.string :any_deliveries_ended_in_neonatal_death

      t.string :woman_required_level_2_hdu_care, comment: 'As determined by the use of an HDU obs chart'
      t.string :hours_of_level_2_hdu_care

      t.string :woman_transferred_to_level_3_icu_care
      t.string :hours_of_level_3_icu_care

      t.string :woman_underwent_hysterectomy
      t.string :woman_died

      t.integer :blood_loss_in_millilitres
      t.string :blood_loss_estimated_or_measured

      t.string :cause_of_bleed_primary
      t.string :cause_of_bleed_secondary

      t.integer :fresh_frozen_plasma_used_in_units
      t.integer :platelets_used_in_units
      t.integer :fibrinogen_used_in_grams
      t.integer :cryoprecipitate_used_in_pools
      t.integer :red_blood_cell_transfused_in_units
      t.integer :recombinant_factor_viia_used_in_micrograms

      t.string :blood_products_were_given_according_to_rotem_protocol

      t.timestamps
    end
  end
end
