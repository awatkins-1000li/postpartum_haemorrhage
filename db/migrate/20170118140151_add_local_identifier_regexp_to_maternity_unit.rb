class AddLocalIdentifierRegexpToMaternityUnit < ActiveRecord::Migration[5.0]
  def change
    add_column :maternity_units, :local_identifier_regexp, :string
  end
end
