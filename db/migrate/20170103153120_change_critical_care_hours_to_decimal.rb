class ChangeCriticalCareHoursToDecimal < ActiveRecord::Migration[5.0]
  def up
    change_column :pph_episodes, :hours_of_level_2_hdu_care, 'decimal(4,1) USING CAST(hours_of_level_2_hdu_care AS decimal(4,1))' #:decimal, precision:4, scale:1 
    change_column :pph_episodes, :hours_of_level_3_icu_care, 'decimal(4,1) USING CAST(hours_of_level_3_icu_care AS decimal(4,1))' #:decimal, precision:4, scale:1
  end

  def down
    change_column :pph_episodes, :hours_of_level_2_hdu_care, :string
    change_column :pph_episodes, :hours_of_level_3_icu_care, :string
  end
end
