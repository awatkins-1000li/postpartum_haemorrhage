class AddCauseOfBleedNoCauseTriggerNotReachedToPPHEpisodes < ActiveRecord::Migration[5.0]
  def change
    change_table :pph_episodes do |t|
      t.boolean :pph_episodes, :cause_of_bleed_no_cause_trigger_not_reached
    end
  end
end
