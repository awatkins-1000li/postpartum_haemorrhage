# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180516105540) do

  create_table "blood_gas_results", force: :cascade do |t|
    t.datetime "test_at"
    t.integer  "haemoglobin_in_grams_per_litre"
    t.decimal  "lactate_in_millimoles_per_litre", precision: 4, scale: 1
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.integer  "pph_episode_id"
    t.index ["pph_episode_id"], name: "index_blood_gas_results_on_pph_episode_id"
  end

  create_table "blood_results", force: :cascade do |t|
    t.datetime "result_at"
    t.integer  "haemoglobin_in_grams_per_litre"
    t.decimal  "lactate_in_millimoles_per_litre",                  precision: 4, scale: 1
    t.integer  "platelets_in_thousand_millions_per_litre"
    t.decimal  "activated_partial_thromboplastin_time_in_seconds", precision: 6, scale: 2
    t.decimal  "prothrombin_time_in_seconds",                      precision: 6, scale: 2
    t.decimal  "fibrinogen_in_grams_per_litre",                    precision: 6, scale: 2
    t.string   "origin"
    t.integer  "pph_episode_id"
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.index ["pph_episode_id"], name: "index_blood_results_on_pph_episode_id"
  end

  create_table "bulletins", force: :cascade do |t|
    t.text     "markdown_text"
    t.datetime "post_at"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "title"
  end

  create_table "lab_test_results", force: :cascade do |t|
    t.datetime "results_available_at"
    t.integer  "haemoglobin_in_grams_per_litre"
    t.integer  "platelets_in_thousand_millions_per_litre"
    t.decimal  "activated_partial_thromboplastin_time_in_seconds", precision: 6, scale: 2
    t.decimal  "prothrombin_time_in_seconds",                      precision: 6, scale: 2
    t.decimal  "fibrinogen_in_grams_per_litre",                    precision: 6, scale: 2
    t.integer  "pph_episode_id"
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.index ["pph_episode_id"], name: "index_lab_test_results_on_pph_episode_id"
  end

  create_table "maternity_units", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "local_identifier_regexp"
  end

  create_table "monthly_stats_updates", force: :cascade do |t|
    t.date     "month"
    t.integer  "obstetric_led_maternities"
    t.integer  "obstetric_led_deliveries"
    t.integer  "alongside_midwifery_led_maternities"
    t.integer  "alongside_midwifery_led_deliveries"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "maternity_unit_id"
    t.integer  "freestanding_midwifery_led_maternities"
    t.integer  "freestanding_midwifery_led_deliveries"
    t.integer  "home_maternities"
    t.integer  "home_deliveries"
    t.index ["maternity_unit_id"], name: "index_monthly_stats_updates_on_maternity_unit_id"
  end

  create_table "pph_episodes", force: :cascade do |t|
    t.datetime "delivered_at"
    t.datetime "discharged_at"
    t.integer  "number_of_deliveries"
    t.string   "type_of_delivery"
    t.string   "any_deliveries_ended_in_neonatal_death"
    t.string   "woman_received_level_2_hdu_care"
    t.decimal  "hours_of_level_2_hdu_care",                                precision: 4, scale: 1
    t.string   "woman_received_level_3_icu_care"
    t.decimal  "hours_of_level_3_icu_care",                                precision: 4, scale: 1
    t.string   "woman_underwent_hysterectomy"
    t.string   "woman_died"
    t.integer  "blood_loss_in_millilitres"
    t.string   "blood_loss_estimated_or_measured"
    t.integer  "fresh_frozen_plasma_used_in_units"
    t.integer  "platelets_used_in_units"
    t.integer  "fibrinogen_used_in_grams"
    t.integer  "cryoprecipitate_used_in_pools"
    t.integer  "red_blood_cell_transfused_in_units"
    t.integer  "recombinant_factor_viia_used_in_micrograms"
    t.string   "blood_products_were_given_according_to_rotem_protocol"
    t.datetime "created_at",                                                                       null: false
    t.datetime "updated_at",                                                                       null: false
    t.string   "local_identifier"
    t.integer  "maternity_unit_id"
    t.string   "haemostatic_uterine_suture_performed"
    t.string   "bakri_balloon_performed"
    t.string   "internal_iliac_artery_ligation_performed"
    t.string   "interventional_radiology_involved"
    t.string   "cell_salvage_used"
    t.integer  "cell_salvage_blood_returned_in_millilitres"
    t.datetime "stage_2_activated_or_litre_lost_at"
    t.boolean  "cause_of_bleed_uterine_atony"
    t.boolean  "cause_of_bleed_surgical"
    t.boolean  "cause_of_bleed_genital_tract_trauma"
    t.boolean  "cause_of_bleed_uterine_rupture"
    t.boolean  "cause_of_bleed_abruption"
    t.boolean  "cause_of_bleed_placenta_praevia"
    t.boolean  "cause_of_bleed_retained_products"
    t.boolean  "cause_of_bleed_placenta_ac_increta"
    t.boolean  "cause_of_bleed_amniotic_fluid_embolism"
    t.boolean  "cause_of_bleed_extragenital_bleed"
    t.boolean  "cause_of_bleed_uterine_inversion"
    t.string   "location_of_delivery"
    t.boolean  "pph_episodes"
    t.boolean  "cause_of_bleed_no_cause_trigger_not_reached"
    t.string   "woman_received_level_2_hdu_care_outside_of_delivery_unit"
    t.string   "four_stage_paperwork_used"
    t.boolean  "confirm_no_rotems"
    t.string   "risk_assessment_completed"
    t.index ["maternity_unit_id"], name: "index_pph_episodes_on_maternity_unit_id"
  end

  create_table "rotem_results", force: :cascade do |t|
    t.datetime "available_at"
    t.integer  "fibtem_a5_in_millimetres"
    t.integer  "extem_a5_in_millimetres"
    t.integer  "intem_a5_in_millimetres"
    t.integer  "extem_ct_in_seconds"
    t.integer  "intem_ct_in_seconds"
    t.integer  "pph_episode_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["pph_episode_id"], name: "index_rotem_results_on_pph_episode_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",     default: 0,  null: false
    t.datetime "locked_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "kerberos_principal"
    t.integer  "maternity_unit_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["maternity_unit_id"], name: "index_users_on_maternity_unit_id"
  end

end
