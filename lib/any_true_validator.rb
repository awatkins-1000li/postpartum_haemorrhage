class AnyTrueValidator < ActiveModel::Validator
  def validate(record)
    unless options[:fields].any?{|attr| record[attr]}
      record.errors.add(:base, "At least one #{options[:one_of]} must be chosen.")
    end
  end
end