module Devise
  module Strategies
    class Kerberos < Authenticatable
      def valid?
        params['user']
      end

      def authenticate!
        resource = mapping.to.where(email:params['user']['email'].downcase).first
        fail(:not_found_in_database) and return unless validate(resource)

        principal = resource.kerberos_principal
        fail(:no_kerberos_principal_for_resource) and return unless principal

        begin
          success!(resource) if KerberosAuthenticator.authenticate!(principal, params['user']['password'])
        rescue KerberosAuthenticator::Error => e
          Rails.logger.warn "Kerberos Authentication failed: #{e}!"
          fail!(:invalid)
        end
      end
    end
  end
end
