wb = xlsx_package.workbook
wb.add_worksheet(name: "PPH Episodes") do |sheet|



  br_cols  = (BloodResult.column_names - ['id', 'created_at', 'updated_at', 'pph_episode_id'])
  bgr_cols = (BloodGasResult.column_names - ['id', 'created_at', 'updated_at', 'pph_episode_id'])
  rot_cols = (ROTEMResult.column_names    - ['id', 'created_at', 'updated_at', 'pph_episode_id'])
  ltr_cols = (LabTestResult.column_names  - ['id', 'created_at', 'updated_at', 'pph_episode_id'])

  sum_stats = [:max, :min]


  sheet.add_row [
    'Unit',
    'Local Identifier',
    'Delivered at',
    'Discharged at',
    'Number of deliveries',
    'Type of Delivery',
    'Location of Delivery',
    'Woman received Level 2 (HDU) care',
    'Hours of Level 2 (HDU) care',
    'Woman received any Level 2 (HDU) care outside of Delivery Unit',
    'Woman received Level 3 (ICU) care',
    'Hours of Level 3 (ICU) care',
    'Blood loss in millilitres',
    'Blood loss ≥ 1000mL',
    'Blood loss ≥ 1500mL',
    'Blood loss ≥ 2000mL',
    'Blood loss ≥ 2500mL',    
    'Blood loss Estimated or Measured?',
    'Stage 2 (or ≥1000ml blood loss) at',
    'Cause of Bleed - Uterine atony',
    'Cause of Bleed - Surgical',
    'Cause of Bleed - Genital tract Trauma',
    'Cause of Bleed - Extragenital bleeding',
    'Cause of Bleed - Uterine Rupture',
    'Cause of Bleed - Placenta praevia',
    'Cause of Bleed - Placenta accreta/increta',
    'Cause of Bleed - Amniotic fluid embolism',
    'Cause of Bleed - Uterine inversion',
    'Cause of Bleed - Abruption',
    'Cause of Bleed - Retained products of conception (including placenta)',
    'Cause of Bleed - No cause - 1000mL trigger not reached',
    'Summary of Cause of Bleed',

    'Cell salvage used?',
    'Cell salvage - blood returned to patient by transfusion in millilitres',

    'Surgical intervention - Haemostatic uterine suture performed?',
    'Surgical intervention - Bakri balloon performed?',
    'Surgical intervention - Internal iliac artery ligation performed?',
    'Interventional radiology involved?',

    'Red Blood Cell (RBC) transfused in Units',
    'Fibrinogen used in grams',
    'Platelets used in Units',
    'Cryoprecipitate used in Units',
    'Fresh Frozen Plasma used in Units',
    'Recombinant factor VIIa (rFVIIa) used in micrograms',
    'Were blood products given according to ROTEM protocol? [Deprecated]',
    'Risk assessment (stage 0) completed?',
    'Four stage paperwork used?',
    'Risk assessment completed?',

    'Woman underwent hysterectomy?',
    'Woman died?',
    'Any deliveries ended in neonatal death? [Deprecated]',

    #'Possible duplicate?',

    'Incomplete sections',
    'Triggers',
    'Number of triggers',
    'Significant episode',

    'Blood Results count',
    'ROTEMs count'

  ] + 
  br_cols.map  {|b| sum_stats.map {|s| "Blood Results - #{s.to_s.humanize} #{b.humanize}"}}.flatten +
  rot_cols.map {|b| sum_stats.map {|s| "ROTEM Results - #{s.to_s.humanize} #{b.humanize}"}}.flatten +
  br_cols.map  {|b| "Blood Results - Latest before Delivery - #{b.humanize}"}.flatten +
  br_cols.map  {|b| "Blood Results - Earliest after Delivery - #{b.humanize}"}.flatten +
  br_cols.map  {|b| "Blood Results - Latest after Delivery - #{b.humanize}"}.flatten +
  ['Pre-bleed Haemoglobin in g/L', 'Discharge Haemoglobin in g/L', 'Discharge Haemoglobin in g/L adjusted for RBC use', 'Haemoglobin drop in g/L adjusted for RBC use']


  @pph_episodes.each do |e|
    sheet.add_row [
      e.maternity_unit.name,
      e.local_identifier,
      e.delivered_at,
      e.discharged_at,
      e.number_of_deliveries,
      e.type_of_delivery,
      e.location_of_delivery,

      e.woman_received_level_2_hdu_care,
      e.hours_of_level_2_hdu_care,
      e.woman_received_level_2_hdu_care_outside_of_delivery_unit,
      e.woman_received_level_3_icu_care,
      e.hours_of_level_3_icu_care,

      e.blood_loss_in_millilitres,
      (not e.blood_loss_in_millilitres.nil? and e.blood_loss_in_millilitres >= 1000),
      (not e.blood_loss_in_millilitres.nil? and e.blood_loss_in_millilitres >= 1500), 
      (not e.blood_loss_in_millilitres.nil? and e.blood_loss_in_millilitres >= 2000),           
      (not e.blood_loss_in_millilitres.nil? and e.blood_loss_in_millilitres >= 2500),
      e.blood_loss_estimated_or_measured,
      e.stage_2_activated_or_litre_lost_at,

      e.cause_of_bleed_uterine_atony,
      e.cause_of_bleed_surgical,
      e.cause_of_bleed_genital_tract_trauma,
      e.cause_of_bleed_extragenital_bleed,
      e.cause_of_bleed_uterine_rupture,
      e.cause_of_bleed_placenta_praevia,
      e.cause_of_bleed_placenta_ac_increta,
      e.cause_of_bleed_amniotic_fluid_embolism,
      e.cause_of_bleed_uterine_inversion,
      e.cause_of_bleed_abruption,
      e.cause_of_bleed_retained_products,
      e.cause_of_bleed_no_cause_trigger_not_reached,
      e.cause_of_bleed_summary,

      e.cell_salvage_used,
      e.cell_salvage_blood_returned_in_millilitres,

      e.haemostatic_uterine_suture_performed,
      e.bakri_balloon_performed,
      e.internal_iliac_artery_ligation_performed,
      e.interventional_radiology_involved,

      e.red_blood_cell_transfused_in_units,
      e.fibrinogen_used_in_grams,
      e.platelets_used_in_units,
      e.cryoprecipitate_used_in_pools,
      e.fresh_frozen_plasma_used_in_units,
      e.recombinant_factor_viia_used_in_micrograms,
      e.blood_products_were_given_according_to_rotem_protocol,
      e.risk_assessment_completed,
      e.four_stage_paperwork_used,
      e.risk_assessment_completed,

      e.woman_underwent_hysterectomy,
      e.woman_died,
      e.any_deliveries_ended_in_neonatal_death,

      #e.possible_duplicate?,

      e.incomplete_sections.map {|sym| sym.to_s.humanize}.join(', '),
      e.triggers.map {|sym| sym.to_s.humanize}.join(', '),
      e.triggers.count,
      e.significant_episode,
      
      e.blood_results.size,
      e.rotem_results.size,

    ] +
    br_cols.map  {|b| sum_stats.map {|s| e.send(s, :blood_results, b.to_sym)}}.flatten +
    rot_cols.map {|b| sum_stats.map {|s| e.send(s, :rotem_results, b.to_sym)}}.flatten +
    br_cols.map  {|b| e.latest_blood_result_before_delivery(b.to_sym)&.send(b.to_sym)} +
    br_cols.map  {|b| e.earliest_blood_result_after_delivery(b.to_sym)&.send(b.to_sym)} +
    br_cols.map  {|b| e.latest_blood_result_after_delivery(b.to_sym)&.send(b.to_sym)} +
    [e.pre_bleed_haemoglobin_in_grams_per_litre, 
     e.discharge_haemoglobin_in_grams_per_litre, 
     e.discharge_haemoglobin_in_grams_per_litre_adjusted_for_rbc_use, 
     e.haemoglobin_drop_in_grams_per_litre_adjusted_for_rbc_use]
    

    #sheet.add_hyperlink({:location => pph_episode_url(e, :path_only => false), :ref => sheet.rows.last.cells[1]})
  end

  wb.add_worksheet(name:"ROTEM Results") do |sheet|
    sheet.add_row [
      'Unit',
      'Local Identifier',
      'Result available at',
      'FIBTEM A5 in millimetres',
      'EXTEM A5 in millimetres',
      'INTEM A5 in millimetres',
      'EXTEM CT in seconds',
      'INTEM CT in seconds'
    ]

    @pph_episodes.each do |e|
      e.rotem_results.each do |r|
        sheet.add_row [
          e.maternity_unit.name,
          e.local_identifier,
          r.available_at,
          r.fibtem_a5_in_millimetres,
          r.extem_a5_in_millimetres,
          r.intem_a5_in_millimetres,
          r.extem_ct_in_seconds,
          r.intem_ct_in_seconds
        ]
      end
    end
  end


  wb.add_worksheet(name:"Blood Results") do |sheet|
      sheet.add_row [
        'Unit',
        'Local Identifier',
      ] + br_cols.map {|c| c.humanize}

      @pph_episodes.each do |e|
        e.blood_results.each do |r|
          sheet.add_row [
            e.maternity_unit.name,
            e.local_identifier,
          ] + br_cols.map {|c| r.send(c)}
        end
      end
    end



  wb.add_worksheet(name:'Monthly Figures') do |sheet|
    jsons = @monthly_stats_updates.map {|msu| msu.as_json.reject {|k,v| [:id, :maternity_unit_id].include? k }}

    sheet.add_row jsons.first.keys.map {|k| k.to_s.humanize.gsub('gte','≥').gsub('gt','>')}

    jsons.each do |j|
      sheet.add_row j.values
    end


  end


end

wb.use_shared_strings = true
