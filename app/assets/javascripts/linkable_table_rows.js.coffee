$(document).ready () ->
  $('table.selectable tr').addClass('acts-as-link')
  $('table.selectable tr').click (e) ->
    if $(e.currentTarget).data('href')
      if Turbolinks
        Turbolinks.visit($(e.currentTarget).data('href'))
      else
        location.href = $(e.currentTarget).data('href') 
