$(document).ready () ->

  if $('[data-high-interest-only]').length
    hasAnyBloodProducts = () ->
      if parseInt($('input#pph_episode_red_blood_cell_transfused_in_units').val()) > 0
        return true

      if parseInt($('input#pph_episode_fibrinogen_used_in_grams').val()) > 0
        return true

      if parseInt($('input#pph_episode_platelets_used_in_units').val()) > 0
        return true

      if parseInt($('input#pph_episode_fresh_frozen_plasma_used_in_units').val()) > 0
        return true

      if parseInt($('input#pph_episode_cryoprecipitate_used_in_pools').val()) > 0
        return true

      if parseInt($('input#pph_episode_recombinant_factor_viia_used_in_micrograms').val()) > 0
        return true
        
      return false
    
    isHighInterest = ()->
      if parseInt($('input#pph_episode_blood_loss_in_millilitres').val()) >= 1500
        return true
        
      if hasAnyBloodProducts()
        return true

      if $('select#pph_episode_woman_underwent_hysterectomy').val().match(/^Yes/)
        return true

      if $('select#pph_episode_woman_died').val().match(/^Yes/)
        return true

      if $('select#pph_episode_woman_received_level_2_hdu_care_outside_of_delivery_unit').val().match(/^Yes/)
        return true
        
      if $('select#pph_episode_woman_received_level_3_icu_care').val().match(/^Yes/)
        return true
      
      return false

    selector = [
      'input#pph_episode_red_blood_cell_transfused_in_units',
      'input#pph_episode_fibrinogen_used_in_grams',
      'input#pph_episode_platelets_used_in_units',
      'input#pph_episode_fresh_frozen_plasma_used_in_units',
      'input#pph_episode_cryoprecipitate_used_in_pools',
      'input#pph_episode_recombinant_factor_viia_used_in_micrograms',
      'input#pph_episode_blood_loss_in_millilitres',
      'select#pph_episode_woman_underwent_hysterectomy',
      'select#pph_episode_woman_died',
      'select#pph_episode_woman_received_level_2_hdu_care_outside_of_delivery_unit',
      'select#pph_episode_woman_received_level_3_icu_care'
    ].join(',')

    updateHighInterest = () ->
      if not isHighInterest()
        $('.pph_episode_form').addClass('not-high-interest')
      else
        $('.pph_episode_form').removeClass('not-high-interest')


    isPre201904 = () ->
      deliveredAt = moment($('input#pph_episode_delivered_at').val(), 'YYYY-MM-DDTHH:mmZ')

      if not deliveredAt.isValid()
        return false

      return (deliveredAt < moment('2019-04-01'))


    updatePre201904 = () ->
      if not isPre201904()
        $('.pph_episode_form').addClass('post-201904')
      else
        $('.pph_episode_form').removeClass('post-201904')

    $(selector).change (e)->
      updateHighInterest()
      updatePre201904()

    $('input#pph_episode_delivered_at').change (e)->
      updateHighInterest()
      updatePre201904()

    updatePre201904()
    updateHighInterest()
            
    updateVisibilityOfHoursInLevel2Care = ()->
      if $('select#pph_episode_woman_received_level_2_hdu_care_outside_of_delivery_unit').val().match(/^Yes/)
        $('input#pph_episode_hours_of_level_2_hdu_care').parent().parent().show()
      else
        $('input#pph_episode_hours_of_level_2_hdu_care').parent().parent().hide()

    $('select#pph_episode_woman_received_level_2_hdu_care_outside_of_delivery_unit').change (e)->
      updateVisibilityOfHoursInLevel2Care()

    updateVisibilityOfHoursInLevel2Care()