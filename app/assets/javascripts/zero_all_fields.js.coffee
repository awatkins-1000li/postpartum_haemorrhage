$(document).ready () ->
  $('.section.blood-products h2').after("
    <button id='zero-blood-products' class='ui button' type='button'>No blood products given - set all to zero</button>")

  $('button#zero-blood-products').click ()->
    $('input#pph_episode_red_blood_cell_transfused_in_units').val(0)
    $('input#pph_episode_fibrinogen_used_in_grams').val(0)
    $('input#pph_episode_platelets_used_in_units').val(0)
    $('input#pph_episode_fresh_frozen_plasma_used_in_units').val(0)
    $('input#pph_episode_cryoprecipitate_used_in_pools').val(0)
    $('input#pph_episode_recombinant_factor_viia_used_in_micrograms').val(0)