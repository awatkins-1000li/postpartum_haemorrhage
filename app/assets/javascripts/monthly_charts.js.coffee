$(document).ready () ->
  if $('.monthly-chart').length
    msu_url = '/monthly_stats_updates.json'

    max_months = 12

    try
      if window.URL?

        by_unit_url = by_unit_url + '?'
        loc = new URL(window.location.href)

        max_months = loc.searchParams.get("max_months")

    if !max_months?
      max_months = 12

    msu_url = msu_url + '?max_months=' + max_months


    $.get msu_url, (msus) ->

      msus = msus.sort (a,b) -> moment(a['month'], "YYYY-MM-DD").unix() - moment(b['month'], "YYYY-MM-DD").unix()

      prettyMonths = msus.map (mu) -> moment(mu['month'], "YYYY-MM-DD").format("MMM YY")

      $('.monthly-chart').each (i,e) ->

        series = $(e).children('.series').map (i, se) ->
          {
            name: $(se).data('name'),
            data: (msus.map (mu) -> mu[$(se).data('field')]),
            color: $(se).data('color'),
            marker:{symbol: 'circle'},
            field: $(se).data('field')
          }

        if series.length == 0
          return

        if series[0].field.startsWith('proportion')
          percentChart = true
          series.each (i, s) ->
            s.data = s.data.map (d) -> Math.round(d*100)

          maxValue = series.map( (i, s) -> s.data ).toArray().reduce( ((a,b) -> a.concat b), [] ).reduce( ((a,b) -> Math.max(a,b)), 0)

          if maxValue > 55
            yMax = Math.max(maxValue, 100)
          

        title = $(e).data('title') || series[0].name

        Highcharts.chart e, {
          title:{
            text: title
          }
          yAxis:{
            title: {text: null},
            allowDecimals: false,
            min: 0,
            max: yMax,
            labels: {format: (if percentChart then '{value}%' else '{value}') }
          },
          tooltip:{
            valueSuffix: (if percentChart then '%' else null)
            },
          xAxis: {
              categories: prettyMonths
              title: {text: null}
            },
          legend: {
            enabled: (series.length > 1),
            verticalAlign: 'top', y:25
            },
          series: series,
          credits: {href: null, text:'Accuracy of data may be limited by completeness.'}
        }
