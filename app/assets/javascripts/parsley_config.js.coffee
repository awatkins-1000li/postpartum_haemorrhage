addWarning = (e)->
  e.addClass('warning')
  e.siblings('.warning-interrobang').remove()
  e.after("<abbr title=\"This value looks unusual (which doesn't mean it's wrong!)\" class='warning-interrobang'>‽</span>")

removeWarning = (e)->
  e.removeClass('warning')
  e.siblings('.warning-interrobang').remove()

considerWarning = (e)->

  inputName = e.get(0).name
  intParsedValue = parseInt(e.val())

  if (/number_of_deliveries/).test(inputName)
    if parseInt(intParsedValue) > 1
      return addWarning(e)

  # Lab results and Blood gas

  if (/result/).test(inputName)
    if (/haemoglobin_in_grams_per_litre/).test(inputName)
      if intParsedValue > 150
        return addWarning(e)

      if intParsedValue < 50
        return addWarning(e)

    if (/lactate_in_millimoles_per_litre/).test(inputName)
      if intParsedValue >= 8
        return addWarning(e)
    
    if (/platelets_in_thousand_millions_per_litre/).test(inputName)
      if intParsedValue < 25
        return addWarning(e)
    
    if (/activated_partial_thromboplastin_time_in_seconds/).test(inputName)
      if intParsedValue < 20
        return addWarning(e)

      if intParsedValue > 40
        return addWarning(e)

    if (/prothrombin_time_in_seconds/).test(inputName)
      if intParsedValue < 9
        return addWarning(e)

      if intParsedValue > 20
        return addWarning(e)

    # ROTEMs

    if (/fibtem_a5_in_millimetres/).test(inputName)
      if intParsedValue > 40
        return addWarning(e)

      if intParsedValue < 10
        return addWarning(e)

    if (/extem_ct_in_seconds/).test(inputName)
      if intParsedValue > 100
        return addWarning(e)

      if intParsedValue < 20
        return addWarning(e)


  removeWarning(e)


$().ready ()->
  $('input').each (i,e)->
    considerWarning($(e))


window.Parsley.on 'field:validated', () ->
  if this.isValid()
    this.$element.popup 'hide'
    this.$element.popup 'destroy'
    considerWarning(this.$element)
    return

  removeWarning(this.$element)

  wrap = (message) ->
    "<p><i class='warning sign icon red'></i>#{message}</p>"

  htmlContent = (wrap msg for msg in this.getErrorsMessages()).join('')

  if this.$element.popup 'exists'
    this.$element.popup('change content', htmlContent)
  else
    this.$element.popup {
      on: 'manual',
      closable: 'false',
      html: htmlContent,
      position: 'bottom left',
      lastResort: 'bottom left',
      maxSearchDepth: 1,
      duration: 50,
      hideOnScroll: false,
      variation: 'flowing error'
    }

  this.$element.popup 'show'
  this.$element.popup 'reposition'

