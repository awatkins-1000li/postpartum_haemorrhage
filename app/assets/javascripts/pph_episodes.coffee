# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready () ->
  $('#pph_episodes_filter select').change (e) ->
    $('#pph_episodes_filter').submit()

  $('#pph_episodes_filter button[type=submit]').hide()