$(document).ready () ->

  extendTableToNextRow = () ->


  makeTableExtending = (t) ->
    $(t).find('tbody tr:gt(0)').each (i, r)->
      if $(r).prev('tr').find('td input').filter(()->
        this.value.length > 0
        ).length == 0
        $(r).hide()

    $(t).find('tbody tr td input').on 'change', (ev)->
      $(this).closest('tr').next('tr').show()


  $('.table.extending').each (i, e) ->
    makeTableExtending e