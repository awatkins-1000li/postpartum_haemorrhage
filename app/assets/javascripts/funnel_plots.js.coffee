$(document).ready () ->
  if $('.funnel-plot').length

    by_unit_url = '/monthly_stats_updates/by_unit.json'


    try
      if window.URL?

        by_unit_url = by_unit_url + '?'
        loc = new URL(window.location.href)

        start = loc.searchParams.get("start")
        if start?
          by_unit_url = by_unit_url + "start=#{start}&"

        end = loc.searchParams.get("end")
        if end?
          by_unit_url = by_unit_url + "end=#{end}&"


    $.get by_unit_url, (by_unit) ->

      data = by_unit.data

      startDate = moment(by_unit.startDate, 'YYYY-MM-DD')
      endDate = moment(by_unit.endDate, 'YYYY-MM-DD')

      prettyPeriod = "#{startDate.format('MMM YYYY')}-#{endDate.format('MMM YYYY')}"

      $('.funnel-plot').each (i,e) ->

        numerator   = $(e).data('numerator')
        denominator = $(e).data('denominator')

        prettyNumerator = $(e).data('pretty-numerator') || numerator
        prettyDenominator = $(e).data('pretty-denominator') || numerator

        finalDenominator = 1000

        title = $(e).data('title') || "#{prettyNumerator} per #{finalDenominator} #{prettyDenominator}<br>#{prettyPeriod}"
        funnelType = $(e).data('funnel-type') || 'rate'

        rate = {}

        series = []

        maxDenom = 0
        maxRate = $(e).data('rate-max') || 0

        minRate = $(e).data('rate-min') || 10000 # HACK

        totalNumerator = 0
        totalDenominator = 0

        for k, v of data[numerator]

          next if data[denominator][k] < 1

          totalNumerator   += data[numerator][k]
          totalDenominator += data[denominator][k]

          rate[k] = (data[numerator][k]*finalDenominator)/data[denominator][k]


          minRate = Math.min(minRate, rate[k])
          maxRate = Math.max(maxRate, rate[k])
          maxDenom = Math.max(maxDenom, data[denominator][k])


          series.push {
            name: k, type: 'scatter',
            data: [ [data[denominator][k], rate[k]] ],
            color: if k == by_unit.thisUnit then '#c68813' else '#33b',
            marker: {symbol: 'circle', radius: if k == by_unit.thisUnit then 5 else 4 },
            tooltip: {headerFormat:"<em>#{k}</em><br>", pointFormat:"#{(rate[k].toPrecision(3))} per #{finalDenominator}<br>(#{data[numerator][k]} for #{data[denominator][k]})"},
            zIndex: if k == by_unit.thisUnit then 200 else 100
          }


        averageRate = (totalNumerator*finalDenominator)/totalDenominator


        upper3 = []
        lower3 = []
        upper2 = []
        lower2 = []


        predictInterval = (x, z)->
          #Poisson, I think
          firstBit  = 2*x*averageRate/finalDenominator+Math.pow(z,2)
          secondBit = z*Math.sqrt(Math.pow(z,2)+4*x*averageRate/finalDenominator*(1-averageRate/finalDenominator))

          return [
           (firstBit+secondBit)/(x+Math.pow(z,2))/2*finalDenominator,
           (firstBit-secondBit)/(x+Math.pow(z,2))/2*finalDenominator 
          ]

        for x in [0..maxDenom*1.05] by (maxDenom/100)

          interval = predictInterval(x, 3)
          upper3.push [x, interval[0]]
          lower3.push [x, interval[1]]

          interval = predictInterval(x, 1.960)
          upper2.push [x, interval[0]]
          lower2.push [x, interval[1]]
        

        minRate = Math.min(minRate, lower3[34][1])
        maxRate = Math.max(maxRate, upper3[34][1])

        maxRate = maxRate * 1.05
        minRate = minRate / 1.05

        if (minRate/(maxRate-minRate)) < 0.45 then minRate = 0

        averageAndLimitsMarkerSettings = {
          enabled: false,
          symbol: 'diamond',
          radius: 2
        }

        series.unshift {
          name: 'Average'
          data: [[0, averageRate], [maxDenom*1.05, averageRate]],
          marker: averageAndLimitsMarkerSettings,
          tooltip: {headerFormat:"", pointFormat:"<em>Avg:</em> #{averageRate.toPrecision(3)} per #{finalDenominator}"},
          color: '#666',
          zIndex: 50
        }

        series.unshift {
          name: 'Upper 3',
          data: upper3,
          marker: averageAndLimitsMarkerSettings,
          tooltip: {headerFormat:"", pointFormat:"<em>3σ/99.7% limit</em> - >99 out 100 expected to fall within limits"},
          color: '#b33',
          zIndex: 25
        }

        series.unshift {
          name: 'Lower 3',
          data: lower3,
          marker: averageAndLimitsMarkerSettings,
          tooltip: {headerFormat:"", pointFormat:"<em>3σ/99.7% limit</em> - >99 out 100 expected to fall within limits"},
          color: '#b33'
          zIndex: 25
        }

        series.unshift {
          name: 'Upper 2',
          data: upper2,
          marker: averageAndLimitsMarkerSettings,
          tooltip: {headerFormat:"", pointFormat:"<em>95% limit</em> - 19 out 20 expected to fall within limits"},
          color: '#e5a9a9',
          zIndex: 25
        }

        series.unshift {
          name: 'Lower 2',
          data: lower2,
          marker: averageAndLimitsMarkerSettings,
          tooltip: {headerFormat:"", pointFormat:"<em>95% limit</em> - 19 out 20 expected to fall within limits"},
          color: '#e5a9a9'
          zIndex: 25
        }


        chart = Highcharts.chart e,{
          title:{
            text: title
          },
          yAxis:{
            title: {text: "#{prettyNumerator} per #{finalDenominator} #{prettyDenominator}"},
            min: minRate,
            max: maxRate,
          },
          xAxis:{
            title: {text: prettyDenominator},
            max: Math.round(maxDenom*1.05)
          },

          legend:{ enabled: false },

          series: series,
          credits: {href: null, text:'Accuracy of data may be limited by completeness.'}
        }
