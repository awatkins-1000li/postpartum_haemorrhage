# https://www.npeu.ox.ac.uk/ukoss/current-surveillance/low-maternal-plasma-fibrinogen

$(document).ready ()->

  fibtemInputs = $("input[name*='rotem_results['][name*='fibtem_a5_in_millimetres']")
  fibrinInputs = $("input[name*='results['][name*='fibrinogen_in_grams_per_litre']")

  isUKOSSLMPFCase = ()->

    dateVal = moment($('#pph_episode_delivered_at').val(),'YYYY-MM-DDTHH:mmZ')

    # Ignore missing dates
    # 1st November 2017 – 31st October 2018
    # (Note that moment months are zero-indexed)
    if dateVal.isValid()
      if dateVal < moment({year:2017, month:10, day:1})
        return false

      if dateVal > moment({year:2018, month:9, day:31})
        return false


    fibrinTrigger = 2

    for el in fibrinInputs
      if $(el).val().length > 0 and $(el).val() < fibrinTrigger
        return true


    fibtemTrigger = 10

    for el in fibtemInputs
      if $(el).val().length > 0 and $(el).val() < fibtemTrigger
        return true

    false


  updateUKOSSLMPFAdvice = ()->
    if not isUKOSSLMPFCase()
      $('[data-ukoss-lmpf-case-only]').each (i, e)->
          $(e).children().hide()
    else
      $('[data-ukoss-lmpf-case-only]').each (i, e)->
          $(e).children().show()

  fibtemInputs.change(updateUKOSSLMPFAdvice)
  fibrinInputs.change(updateUKOSSLMPFAdvice)
  $('#pph_episode_delivered_at').change(updateUKOSSLMPFAdvice)

  updateUKOSSLMPFAdvice()
