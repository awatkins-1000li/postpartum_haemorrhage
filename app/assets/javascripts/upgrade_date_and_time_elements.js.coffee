$(document).ready () ->

  formatForDisplay = {}
  formatForDisplay['month'] = 'MMMM YYYY'
  formatForDisplay['date'] = 'DD-MMM-YYYY'
  formatForDisplay['time'] = 'HH:mm'
  formatForDisplay['datetime'] = 'ddd DD-MMM-YYYY HH:mm'

  formatForServer = {}
  formatForServer['month'] = 'YYYY-MM-DD' # Stored as a full date
  formatForServer['date'] = 'YYYY-MM-DD'
  formatForServer['time'] = 'HHmm'
  formatForServer['datetime'] = 'YYYY-MM-DDTHH:mmZ'

  formatsToAccept = {}
  formatsToAccept['month'] = ['YYYY-MM','MMM YYYY', 'MMMM YYYY', 'MM YYYY']
  formatsToAccept['date'] = [formatForDisplay['date']]
  formatsToAccept['time'] = [formatForDisplay['time']]
  formatsToAccept['datetime'] = ['DD-MM-YYYY HHmm', formatForDisplay['datetime'], 'YYYY-MM-DD HHmm']


  if window.Parsley
    addV = (type) ->
      unless window.Parsley._validatorRegistry.validators["#{type}-upgraded"]
        window.Parsley.addValidator "#{type}-upgraded", {
          requirementType: 'boolean',
          messages: {
            en: "This value should be a valid #{type}."
            },
          validateString: (value, requirement) ->
            moment(value, formatsToAccept[type], false).isValid()
          }

    addV('month')
    addV('date')
    addV('time')
    addV('datetime')


  upgradeInputElement = (oldInput) ->
    oldInput = $(oldInput)

    accept  = formatsToAccept[oldInput.data('type')]
    display = formatForDisplay[oldInput.data('type')]
    server  = formatForServer[oldInput.data('type')]

    #return if not (accept and display and server)

    # Create new input element
    newInput = oldInput.clone()

    if newInput.attr('id')
      newInput.attr('id', "#{oldInput.attr('id')}-upgraded")
  
    newInput.attr('name', '')

    newInput.attr('type', 'text')

    # TODO: Set placeholder
    newInput.attr('placeholder', accept[0])

    # Set initial value
    if oldInput.data('type') == 'datetime' and oldInput.val().length > 0 and oldInput.val().charAt(oldInput.val().length-1) != 'Z'
      oldInput.val("#{oldInput.val()}Z")

    initialValue = moment(oldInput.val(), [server, moment.ISO_8601], true)
    if initialValue.isValid()
      newInput.val(initialValue.format(display)) 
      newInput.data('isValid', true)
    else
      newInput.data('isValid', false)

    # Hook up change events
    newInput.on 'change', (e) ->
      newValue = moment(newInput.val(), accept, false)

      if newValue.isValid()
        oldInput.val(newValue.format(server))
        newInput.val(newValue.format(display))
      else
        oldInput.val('')

      oldInput.trigger('change')

    # Hide the old input element and insert the new one
    oldInput.after(newInput)
    oldInput.hide()

    # Re-assign any labels
    $("label[for='#{oldInput.attr('id')}']").attr('for', newInput.attr('id'))

    newInput.parsley(trigger:'focusout keyup', 'uiEnabled': false, validationThreshold:0, debounce:400, "#{oldInput.data('type')}-upgraded":true)

  upgradeTimeElement = (element) ->
    element = $(element)

    display = formatForDisplay[element.data('type')]

    element.attr('datetime', element.text())

    value = moment(element.text(), moment.ISO_8601, true)
    element.text(value.format(display)) if value.isValid()

  $('time').each (i, e) ->
    upgradeTimeElement e

  $("input[data-type='datetime']").each (i, e) ->
    upgradeInputElement e

  $("input[data-type='date']").each (i, e) ->
    upgradeInputElement e

  $("input[data-type='month']").each (i, e) ->
    upgradeInputElement e

