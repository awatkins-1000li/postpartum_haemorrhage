$(document).ready () ->

  $('#pph_episode_woman_received_level_2_hdu_care').change (e)->
    if $(e.currentTarget).val() == 'No'
      $('#pph_episode_hours_of_level_2_hdu_care').val('0') 
      $('#pph_episode_woman_received_level_2_hdu_care_outside_of_delivery_unit').val('No').change()


  $('#pph_episode_woman_received_level_3_icu_care').change (e)->
    if $(e.currentTarget).val() == 'No'
      $('#pph_episode_hours_of_level_3_icu_care').val('0') 

  $('#pph_episode_cell_salvage_used').change (e)->
    if $(e.currentTarget).val() == 'No'
      $('#pph_episode_cell_salvage_blood_returned_in_millilitres').val('0') 