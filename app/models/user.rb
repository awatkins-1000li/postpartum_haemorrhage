class User < ApplicationRecord
  belongs_to :maternity_unit

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :rememberable, :trackable, :lockable

  validates :kerberos_principal, presence: true, if: Proc.new {|u| u.password.nil? and u.encrypted_password.nil?}
  
  def password_required?
    return false if kerberos_principal
    super
  end

end
