class Bulletin < ApplicationRecord
  validates_datetime :post_at


  def self.recent_bulletin
    Bulletin.where('post_at <= ?', DateTime.now).where('post_at >= ?', DateTime.now - 5.days).order(post_at: :desc).first
  end


  def html
    renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML.new(safe_links_only: true, filter_html: true, no_styles:true), autolink: true)
    renderer.render(markdown_text)
  end

end
