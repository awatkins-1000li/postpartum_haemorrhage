class LabTestResult < ApplicationRecord
  belongs_to :pph_episode

  validates_datetime :results_available_at, on_or_before: lambda { DateTime.now }, on_or_after: lambda { 4.years.ago }

  PERMITTED_PARAMS = [:results_available_at, :haemoglobin_in_grams_per_litre, 
    :platelets_in_thousand_millions_per_litre, :activated_partial_thromboplastin_time_in_seconds,
    :prothrombin_time_in_seconds, :fibrinogen_in_grams_per_litre]

  validates :haemoglobin_in_grams_per_litre, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 999, allow_nil:true}
  validates :platelets_in_thousand_millions_per_litre, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 999, allow_nil:true}
  validates :activated_partial_thromboplastin_time_in_seconds, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 199, allow_nil:true}
  validates :prothrombin_time_in_seconds, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 199, allow_nil:true}
  validates :fibrinogen_in_grams_per_litre, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 19, allow_nil:true}

  # last three, precision of two points

  # Ante-natal results? first two only

  # Split into FBC and coagulation-screen
  # Consider rejected samples - response available at

end
