class MonthlyStatsUpdate < ApplicationRecord

  belongs_to :maternity_unit

  default_scope { order('month DESC') } 

  validates_date :month, :before => :today

  validates :obstetric_led_maternities, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 30000}
  validates :obstetric_led_deliveries, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 30000}

  validates :alongside_midwifery_led_maternities, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 30000}
  validates :alongside_midwifery_led_deliveries, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 30000}

  validates :freestanding_midwifery_led_maternities, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 30000}
  validates :freestanding_midwifery_led_deliveries, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 30000}

  validates :home_maternities, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 30000}
  validates :home_deliveries, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 30000}

  [:obstetric_led_maternities, :obstetric_led_deliveries,
   :alongside_midwifery_led_maternities, :alongside_midwifery_led_deliveries,
   :freestanding_midwifery_led_maternities, :freestanding_midwifery_led_deliveries,
   :home_maternities, :home_deliveries].each do |m|

    define_method(m) {
      self.maternity_unit_id == 999 ? MonthlyStatsUpdate.where('month = ?', self.month).where('maternity_unit_id != 999').sum(m) : self[m]
    }

  end



  #has_many :pph_episodes, -> (monthly_stats_update) { where('pph_episodes.delivered_at >= ?', monthly_stats_update.month).where('pph_episodes.delivered_at < ?', monthly_stats_update.month.next_month) }, through: :maternity_unit

  def pph_episodes
    
    if maternity_unit_id == 999
      return @pph_episodes ||= PPHEpisode.where('pph_episodes.delivered_at >= ?', self.month).where('pph_episodes.delivered_at < ?', self.month.next_month)
    end

    @pph_episodes ||= maternity_unit.pph_episodes.where('pph_episodes.delivered_at >= ?', self.month).where('pph_episodes.delivered_at < ?', self.month.next_month)
  end


  #def cache_key
  #  "msu/#{maternity_unit_id}-#{self.month.iso8601}-#{[pph_episodes.maximum(:updated_at), self.updated_at].compact.max}"
  #end

  #def pphs_gte_1000ml
  #  pph_episodes.where('blood_loss_in_millilitres >= ?', 1000).count
  #end

  #def pphs_gte_1500ml
  #  pph_episodes.where('blood_loss_in_millilitres >= ?', 1500).count
  #end

  #def pphs_gte_2500ml
  #  pph_episodes.where('blood_loss_in_millilitres >= ?', 2500).count
  #end

  def episodes_measured
    pph_episodes.where(blood_loss_estimated_or_measured:'Measured').count
  end

  def as_json(options = nil)
    #Rails.cache.fetch("#{cache_key}/json") do

    hsh = super(options)

    hsh[:unit] = maternity_unit.name

    pphs_as_a =  pph_episodes.eager_load(:rotem_results, :blood_gas_results, :lab_test_results, :blood_results).all.to_a

    hsh[:total_episodes] = pph_episodes.count


    [:episodes_measured].each do |k|
      hsh[k] = self.send(k)
      hsh["proportion_#{k.to_s}".to_sym] = hsh[k].to_f/hsh[:total_episodes]
    end




    hsh[:episodes_gte_2rbc] = pphs_as_a.count {|e| e.red_blood_cell_transfused_in_units and e.red_blood_cell_transfused_in_units >= 2}
    hsh[:episodes_gte_5rbc] = pphs_as_a.count {|e| e.red_blood_cell_transfused_in_units and e.red_blood_cell_transfused_in_units >= 5}

    hsh[:proportion_episodes_gte_2rbc] = hsh[:episodes_gte_2rbc].to_f/hsh[:total_episodes]
    hsh[:proportion_episodes_gte_5rbc] = hsh[:episodes_gte_5rbc].to_f/hsh[:total_episodes]

    # CAVEAT on every chart

    # Total Episodes (Number of)
    # Total Maternities (Number of)

    # Blood loss measurement - number and %

    # PPH rate
    # RBC >= 2, >=5

    # Blood products

    # Bar chart of causes

    # FIbrinogen given only clotting 


    hsh[:episodes_with_incomplete_sections] = pphs_as_a.count {|e| e.incomplete_sections.any? }
    hsh[:episodes_with_no_incomplete_sections] = hsh[:total_episodes] - hsh[:episodes_with_incomplete_sections]


    hsh[:total_deliveries]  = 0 + (obstetric_led_deliveries or 0)  + (freestanding_midwifery_led_deliveries or 0)  + (alongside_midwifery_led_deliveries or 0)  + (home_deliveries or 0)
    hsh[:total_maternities] = 0 + (obstetric_led_maternities or 0) + (freestanding_midwifery_led_maternities or 0) + (alongside_midwifery_led_maternities or 0) + (home_maternities or 0)
  
    #[:pphs_gte_1000ml, :pphs_gte_1500ml, :pphs_gte_2500ml].each do |k|
    #  hsh["#{k}_per_1000_maternities".to_sym] = (hsh[k].to_f*1000/hsh[:total_maternities]).round(2)
    #end


    [1000, 1500, 2000, 2500].each do |lim|
      ['gt', 'gte'].each do |cmp|

        str = "pphs_#{cmp}_#{lim}ml"
        cmp2 = (cmp == 'gt' ? '>' : '>=')

        hsh[str.to_sym] = pph_episodes.where("blood_loss_in_millilitres #{cmp2} ?", lim).count
        hsh["proportion_#{str}".to_sym] = hsh[str.to_sym].to_f/hsh[:total_episodes]
        hsh["#{str}_per_1000_maternities".to_sym] = (hsh[str.to_sym].to_f*1000/hsh[:total_maternities]).round(2)

      end
    end

    hsh[:episodes_with_rotems] = pphs_as_a.count {|e| e.rotem_results.any?}

    hsh[:episodes_with_any_level_2_hdu_care_outside_of_delivery_unit] = pphs_as_a.count {|e| e.woman_received_level_2_hdu_care_outside_of_delivery_unit =~ /\AYes/ }
    hsh[:episodes_with_any_level_3_icu_care] = pphs_as_a.count {|e| e.woman_received_level_3_icu_care =~ /\AYes/ }

    hsh[:episodes_with_risk_assessment] = pphs_as_a.count {|e| e.risk_assessment_completed =~ /\AYes/ }
    hsh[:proportion_with_risk_assessment] = hsh[:episodes_with_risk_assessment].to_f/hsh[:total_episodes]


    [ :red_blood_cell_transfused_in_units,
      :recombinant_factor_viia_used_in_micrograms, 
      :fibrinogen_used_in_grams, :platelets_used_in_units, 
      :cryoprecipitate_used_in_pools, :fresh_frozen_plasma_used_in_units, 
      :recombinant_factor_viia_used_in_micrograms ].each do |p|
      hsh[p] = pphs_as_a.map {|e| e.send(p) or 0}.inject(:+)
    end

    hsh

    #end
  end


end
