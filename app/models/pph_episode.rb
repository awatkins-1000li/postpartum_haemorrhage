class PPHEpisode < ApplicationRecord

  default_scope { order('delivered_at DESC').eager_load(:maternity_unit).eager_load(:blood_results).eager_load(:blood_gas_results).eager_load(:lab_test_results).eager_load(:rotem_results) } 

  belongs_to :maternity_unit, required:true

  has_many :blood_gas_results, dependent: :destroy
  has_many :lab_test_results, dependent: :destroy

  has_many :blood_results, dependent: :destroy
  has_many :rotem_results, dependent: :destroy

  validates_datetime :delivered_at,  on_or_before: lambda { DateTime.now }, on_or_after: lambda { 4.years.ago }
  validates_datetime :discharged_at, allow_blank:true, on_or_before: lambda { DateTime.now }, on_or_after: :delivered_at, on_or_before: lambda { |r| r.delivered_at + 3.months  }
  validates_datetime :stage_2_activated_or_litre_lost_at, allow_blank:true, on_or_before: :today, on_or_after: lambda { 4.years.ago }

  validates :local_identifier, presence: true

  validates_each :local_identifier do |record, attribute, value|
    if record.maternity_unit and record.maternity_unit.local_identifier_regexp then
      record.errors.add(attribute, 'must be in the correct format for this Maternity Unit') if not record.maternity_unit.local_identifier_regexp =~ value
    end
  end

  SECTIONS = {
    basic_details: [:local_identifier, :delivered_at, :type_of_delivery, :location_of_delivery], #:discharged_at, :number_of_deliveries, removed for April 2019+
    critical_care: [:woman_received_level_2_hdu_care_outside_of_delivery_unit, :woman_received_level_3_icu_care, :hours_of_level_3_icu_care],
    blood_loss: [:blood_loss_in_millilitres, :blood_loss_estimated_or_measured],
    surgery_and_interventional_radiology: [:haemostatic_uterine_suture_performed, :bakri_balloon_performed, :internal_iliac_artery_ligation_performed, :interventional_radiology_involved],
    products_used: [:recombinant_factor_viia_used_in_micrograms, :fibrinogen_used_in_grams, :platelets_used_in_units, :cryoprecipitate_used_in_pools, :fresh_frozen_plasma_used_in_units, :recombinant_factor_viia_used_in_micrograms],
    outcomes: [:woman_underwent_hysterectomy, :woman_died]
  }

  CAUSES_OF_BLEED = [
    :cause_of_bleed_uterine_atony,
    :cause_of_bleed_surgical,
    :cause_of_bleed_genital_tract_trauma,
    :cause_of_bleed_uterine_rupture,
    :cause_of_bleed_uterine_inversion,
    :cause_of_bleed_abruption,
    :cause_of_bleed_placenta_praevia,
    :cause_of_bleed_retained_products,
    :cause_of_bleed_placenta_ac_increta,
    :cause_of_bleed_amniotic_fluid_embolism,
    :cause_of_bleed_extragenital_bleed,
    :cause_of_bleed_no_cause_trigger_not_reached #FIXME: Logic around this should be improved
    ]

  CHOICE_OF_YES_UNRELATED_NO_UNKNOWN = [
    'Yes, for reasons related to PPH', 
    'Yes, but for reasons unrelated to PPH',
    'No',
    'Unknown'
    ]

  CHOICE_OF_YES_NO_UNKNOWN = [
    'Yes', 'No', 'Unknown'
  ]

  CHOICE_OF_DELIVERY_TYPE = [
    'Spontaneous vaginal delivery',
    'Instrumental',
    'Elective (Category 4) lower segment caesarean section',
    'Category 3 lower segment caesarean section',
    'Category 2 lower segment caesarean section',
    'Category 1 lower segment caesarean section',
    'Other'
  ]

  CHOICE_OF_DELIVERY_LOCATION = [
    'Consultant-led Unit',
    'Alongside Midwifery-led Unit',
    'Freestanding Midwifery-led Unit',
    'Home'
  ]

  CHOICE_OF_ESTIMATED_MEASURED = [
    'Estimated', 'Measured', 'Unknown'
  ]

  CHOICE_OF_INTERVENTIONAL_RADIOLOGY_INVOLVED = [
    'Yes',
    'No',
    'Unknown',
    'No, not required',
    'No, service not available',
    'Yes, advice only',
    'Yes, performed planned procedure ante-partum',
    'Yes, performed unplanned procedure ante-partum',
    'Yes, performed planned procedure post-partum',
    'Yes, performed unplanned procedure post-partum'
  ]



  [
    {
      fields: [:any_deliveries_ended_in_neonatal_death, 
        :woman_received_level_2_hdu_care, :woman_received_level_3_icu_care,
        :woman_underwent_hysterectomy, :woman_died], 
      choices: CHOICE_OF_YES_UNRELATED_NO_UNKNOWN
    },
    {fields: [:type_of_delivery], choices: CHOICE_OF_DELIVERY_TYPE},
    {fields: [:location_of_delivery], choices: CHOICE_OF_DELIVERY_LOCATION},
    {fields: [:blood_loss_estimated_or_measured], choices: CHOICE_OF_ESTIMATED_MEASURED},
    {fields: [:woman_received_level_2_hdu_care_outside_of_delivery_unit,
              :blood_products_were_given_according_to_rotem_protocol, 
              :haemostatic_uterine_suture_performed, 
              :bakri_balloon_performed, 
              :internal_iliac_artery_ligation_performed,
              :risk_assessment_completed, :four_stage_paperwork_used,
              :cell_salvage_used], choices:CHOICE_OF_YES_NO_UNKNOWN},
    {fields: [:interventional_radiology_involved], choices:CHOICE_OF_INTERVENTIONAL_RADIOLOGY_INVOLVED}
  ].each do |fc|

    validates_each fc[:fields] do |record, attr, value|
      record.errors.add(attr, "must be one of #{fc[:choices].inspect}") if not fc[:choices].include?(value) and not value.blank?
    end
  end

  validates :number_of_deliveries,        numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 9}, allow_blank:true
  
  validates :hours_of_level_2_hdu_care,   numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: (4*7*24)}, allow_blank:true
  validates :hours_of_level_3_icu_care,   numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: (4*7*24)}, allow_blank:true
  
  validates :blood_loss_in_millilitres,   numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 9999}, allow_blank:true

  validates :cell_salvage_blood_returned_in_millilitres,   numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 10000}, allow_blank:true
  
  validates :fresh_frozen_plasma_used_in_units,          numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 99}, allow_blank:true
  validates :platelets_used_in_units,                    numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 99}, allow_blank:true
  validates :fibrinogen_used_in_grams,                   numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 99}, allow_blank:true
  validates :cryoprecipitate_used_in_pools,              numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 99}, allow_blank:true
  validates :red_blood_cell_transfused_in_units,         numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 99}, allow_blank:true
  validates :recombinant_factor_viia_used_in_micrograms, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 99}, allow_blank:true


  def any_errors?
    errors.any? or blood_gas_results.any? { |bgr| bgr.errors.any? } or lab_test_results.any? { |ltr| ltr.errors.any? } or rotem_results.any? { |rtm| rtm.errors.any? } or blood_results.any? { |br| br.errors.any? }
  end
  
  def any_blood_products?
     [red_blood_cell_transfused_in_units, fibrinogen_used_in_grams, 
       platelets_used_in_units, cryoprecipitate_used_in_pools,
       fresh_frozen_plasma_used_in_units, recombinant_factor_viia_used_in_micrograms].any? { |bp| (not bp.nil?) and bp > 0  }  
  end

  def cause_of_bleed_summary
    CAUSES_OF_BLEED.find_all {|c| self.send(c) == true }.map {|c| c.to_s.gsub(/\Acause_of_bleed/, '').humanize}.join(', ')
  end
  
  def is_high_interest?
    return true if not blood_loss_in_millilitres.nil? and blood_loss_in_millilitres >= 1500
    return true if any_blood_products?
    return true if woman_underwent_hysterectomy =~ /\AYes/
    return true if woman_died =~ /\AYes/
    return true if woman_received_level_2_hdu_care_outside_of_delivery_unit =~ /\AYes/
    return true if woman_received_level_3_icu_care =~ /\AYes/
    
    return false
  end

  def is_post_201904?
    return true if delivered_at and delivered_at >= DateTime.new(2019,04,01)
    return false
  end

  def incomplete_sections
    retval = []

    SECTIONS.each_pair do |sect, fields|
      next if [:surgery_and_interventional_radiology, :products_used].include?(sect) and not is_high_interest?
      retval << sect if fields.any? { |f| self[f].blank? }
    end
    
    if is_high_interest?
      retval << :cause_of_bleed unless CAUSES_OF_BLEED.any? { |cob| self[cob] }
    end

    return retval
  end

  def triggers
    retval = []

    retval << :blood_loss if (not blood_loss_in_millilitres.nil?) and blood_loss_in_millilitres >= 1000
    
    retval << :blood_products if any_blood_products?

    retval << :rotem if rotem_results.any?

    retval << :itu if (woman_received_level_3_icu_care =~ /\AYes/)

    retval << :hysterectomy if woman_underwent_hysterectomy =~ /\AYes/

    retval << :died if woman_died =~ /\AYes/

    retval
  end

  def min_pre_bleed_haemoglobin_in_grams_per_litre
    return nil if stage_2_activated_or_litre_lost_at.nil?
    pre_bleed_results = blood_results.find_all {|r| (not r.origin =~ /^blood_gas/) and r.result_at and r.result_at <= stage_2_activated_or_litre_lost_at }
    return pre_bleed_results.map {|r| r.haemoglobin_in_grams_per_litre}.compact.min
  end

  def highest_extem_ct_in_seconds
    return rotem_results.map {|r| r.extem_ct_in_seconds}.compact.max
  end

  def max(table, field)
    self.send(table).map {|r| r.send(field)}.compact.max
  end

  def min(table, field)
    self.send(table).map {|r| r.send(field)}.compact.min
  end

  def possible_duplicate?
    maternity_unit.pph_episodes.where(local_identifier:self.local_identifier).count > 1
  end


  def latest_blood_result_before_delivery(with_field = nil)
    pre_delivery_results = blood_results.find_all {|r| r.result_at and r.result_at < self.delivered_at}

    if with_field
      pre_delivery_results = pre_delivery_results.find_all {|r| not r.send(with_field).nil?}
    end

    pre_delivery_results.sort_by {|r| r.result_at}.last
  end

  def earliest_blood_result_after_delivery(with_field = nil)
    post_delivery_results = blood_results.find_all {|r| r.result_at and r.result_at >= self.delivered_at}

    if with_field
      post_delivery_results = post_delivery_results.find_all {|r| not r.send(with_field).nil?}
    end

    post_delivery_results.sort_by {|r| r.result_at}.first
  end

  def latest_blood_result_after_delivery(with_field = nil)
    post_delivery_results = blood_results.find_all {|r| r.result_at and r.result_at >= self.delivered_at}

    if with_field
      post_delivery_results = post_delivery_results.find_all {|r| not r.send(with_field).nil?}
    end

    post_delivery_results.sort_by {|r| r.result_at}.last
  end


  # Hb drop fields

  def pre_bleed_haemoglobin_in_grams_per_litre
   
    possible_results = blood_results.find_all {|r| r.result_at and 
      r.result_at < self.delivered_at and r.result_at >= (self.delivered_at-28.days) and
      not r.haemoglobin_in_grams_per_litre.nil?}

    return nil if possible_results.empty?
    possible_results.sort_by {|r| r.result_at}.first.haemoglobin_in_grams_per_litre

  end

  def discharge_haemoglobin_in_grams_per_litre
    
    possible_results = blood_results.find_all {|r| r.result_at and
      r.result_at > (self.delivered_at+24.hours) and
      not r.haemoglobin_in_grams_per_litre.nil?}


    return nil if possible_results.empty?
    possible_results.sort_by {|r| r.result_at}.last.haemoglobin_in_grams_per_litre

  end

  def discharge_haemoglobin_in_grams_per_litre_adjusted_for_rbc_use

    dhb = discharge_haemoglobin_in_grams_per_litre
    rbc_use = red_blood_cell_transfused_in_units

    return nil if dhb.nil? or rbc_use.nil?

    dhb - (10*rbc_use)
  end


  def haemoglobin_drop_in_grams_per_litre_adjusted_for_rbc_use
    pre = pre_bleed_haemoglobin_in_grams_per_litre
    post = discharge_haemoglobin_in_grams_per_litre_adjusted_for_rbc_use

    return nil if pre.nil? or post.nil?
    
    pre - post
   
  end

  # Important to Kathryn 
    # - Deaths or hysterectorises
    # - >= 2.5L
    # - Any episode where EXTEM CT >= 75
    # - Any use of fibrinogen
    # - Any level 3 care


  def significant_episode
    return true if blood_loss_in_millilitres and blood_loss_in_millilitres >= 2500
    return true if triggers.include? :died or triggers.include? :hysterectomy
    return true if rotem_results.any? {|rr| rr.extem_ct_in_seconds and rr.extem_ct_in_seconds >= 75}
    return true if fibrinogen_used_in_grams and fibrinogen_used_in_grams > 0
    return true if fresh_frozen_plasma_used_in_units and fresh_frozen_plasma_used_in_units > 0
    return true if (woman_received_level_3_icu_care =~ /\AYes/)
    return false
  end

end
