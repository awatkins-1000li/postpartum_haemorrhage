class Ability
  include CanCan::Ability

  def initialize(user)

    if user.maternity_unit.id == 999 then
      can [:read], Bulletin

      can [:create, :update, :destroy], Bulletin do |bulletin|
        bulletin.post_at >= (DateTime.now - 5.minutes) or bulletin.updated_at >= (DateTime.now - 2.hours)
      end


      can [:create, :read, :update, :destroy], PPHEpisode    
      can [:read], MonthlyStatsUpdate
    else
      can [:create, :read, :update, :destroy], MonthlyStatsUpdate, maternity_unit_id: user.maternity_unit_id
      can [:create, :read, :update, :destroy], PPHEpisode, maternity_unit_id: user.maternity_unit_id

      can [:read], Bulletin do |bulletin|
        bulletin.post_at <= DateTime.now
      end
    end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
