class MaternityUnit < ApplicationRecord
  has_many :pph_episodes
  has_many :monthly_stats_updates
  has_many :users

  def local_identifier_regexp
    self[:local_identifier_regexp] ? Regexp.new(self[:local_identifier_regexp]) : nil
  end

  def implied_monthly_stats_updates

    msus = self.monthly_stats_updates.to_a

    month = first_month

    while month < Date.today.beginning_of_month
      unless msus.any? {|msu| msu.month == month}
        msus << MonthlyStatsUpdate.new(maternity_unit:self, month:month)
      end

      month = month.next_month
    end

    msus.sort_by! {|msu| msu.month}.reverse!

    return msus
  end


  def first_month

    candidates = []

    if self.monthly_stats_updates.any?
      candidates << self.monthly_stats_updates.last.month
    end

    if self.pph_episodes.any?
      candidates << self.pph_episodes.last.delivered_at.beginning_of_month
    end

    candidates << Date.today.beginning_of_month.last_month

    candidates << Date.new(2016,12,1)

    [candidates.min, Date.new(2013,1,1)].max
  end

  def any_possible_duplicates?
    grouped = PPHEpisode.unscoped.where(maternity_unit_id:self.id).select(:local_identifier).group(:local_identifier)
    grouped.having('count(*) > 1').any?
  end

end
