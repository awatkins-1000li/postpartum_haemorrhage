class ROTEMResult < ApplicationRecord
  belongs_to :pph_episode

  validates_datetime :available_at, on_or_before: lambda { DateTime.now }, on_or_after: lambda { 4.years.ago }

  PERMITTED_PARAMS = [:available_at, :fibtem_a5_in_millimetres, :extem_a5_in_millimetres, :intem_a5_in_millimetres,
    :extem_ct_in_seconds, :intem_ct_in_seconds]

  validates :fibtem_a5_in_millimetres, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 99, allow_nil:true}
  validates :extem_a5_in_millimetres, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 99, allow_nil:true}
  validates :intem_a5_in_millimetres, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 199, allow_nil:true}

  validates :extem_ct_in_seconds, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 499, allow_nil:true}
  validates :intem_ct_in_seconds, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 499, allow_nil:true}

  # FIXME: ROTEM Warnings
  # fibtem_a5_in_millimetres, >40, <10
  # extem_ct_in_seconds, <20, >100
  # This value is unusual, outlier.


  # group extem and intem, after fibtem
  # allow missing values
  
end
