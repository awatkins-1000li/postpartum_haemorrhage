class BloodGasResult < ApplicationRecord
  belongs_to :pph_episode

  validates_datetime :test_at, on_or_before: lambda { DateTime.now }, on_or_after: lambda { 4.years.ago }

  PERMITTED_PARAMS = [:test_at, :haemoglobin_in_grams_per_litre, :lactate_in_millimoles_per_litre]

  validates :haemoglobin_in_grams_per_litre,  numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 999, allow_nil:true}
  validates :lactate_in_millimoles_per_litre, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 19, allow_nil:true}

end
