class HomeController < ApplicationController

  def home
    @bulletin = Bulletin.recent_bulletin

    authorize! :read, @bulletin if @bulletin

  end

  def charts
  end

end