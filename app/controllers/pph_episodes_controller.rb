class PPHEpisodesController < ApplicationController

  check_authorization
  load_and_authorize_resource :except => :new_or_find

  def new
    #load_and_authorize_resource magic!
    @pph_episode.local_identifier = params['local_identifier']
  end

  def new_or_find

    if params['local_identifier'].nil? or params['local_identifier'].empty? then
      flash[:warning] = 'Please enter a local identifier to add a new episode.'
      authorize! :index, PPHEpisode
      redirect_to pph_episodes_url
      return
    end

    if current_user.maternity_unit.local_identifier_regexp and not current_user.maternity_unit.local_identifier_regexp =~ params['local_identifier']
      @pph_episodes = PPHEpisode.accessible_by(current_ability)
      flash[:alert] = "That didn't seem to be a valid local identifier for this unit. If you believe that this is a mistake, please contact adam.watkins@wales.nhs.uk ."
      authorize! :index, PPHEpisode
      redirect_to pph_episodes_url
      return
    end

    @pph_episodes = PPHEpisode.accessible_by(current_ability).where(local_identifier:params['local_identifier'])
    if @pph_episodes.any?
      authorize! :index, PPHEpisode
      render :index
    else
      authorize! :new, PPHEpisode
      redirect_to new_pph_episode_path(local_identifier:params['local_identifier'])
    end
  end

  def index
    #load_and_authorize_resource magic!

    available_options = [:missing_only, :reverse, :possible_duplicates_only, :hysterectomy_or_death_only, :with_blood_products_only]

    @options = {}

    available_options.each do |opt|
      @options[opt] = params[opt.to_s] || session["pph_episodes-#{opt.to_s}"]
    end

    @options[:reverse] = false unless ['true'].include? @options[:reverse]
    @options[:missing_only] = false unless ['true', 'except_blood_loss'].include? @options[:missing_only]
    @options[:possible_duplicates_only] = false unless ['true'].include? @options[:possible_duplicates_only]
    @options[:hysterectomy_or_death_only] = false unless ['true'].include? @options[:hysterectomy_or_death_only]
    @options[:with_blood_products_only] = false unless ['true'].include? @options[:with_blood_products_only]

    available_options.each do |opt|
      session["pph_episodes-#{opt.to_s}"] = @options[opt]
    end
    
    if request.format == 'xlsx'
       @pph_episodes
    end

    @pph_episodes = @pph_episodes.reverse if @options[:reverse]
    @pph_episodes = @pph_episodes.find_all {|e| e.incomplete_sections.any?} if @options[:missing_only] == 'true'
    @pph_episodes = @pph_episodes.find_all {|e| (e.incomplete_sections - [:blood_loss]).any?} if @options[:missing_only] == 'except_blood_loss'
    @pph_episodes = @pph_episodes.find_all {|e| e.possible_duplicate?} if @options[:possible_duplicates_only] == 'true'
    @pph_episodes = @pph_episodes.find_all {|e| e.woman_underwent_hysterectomy =~ /\AYes/ or e.woman_died =~ /\AYes/} if @options[:hysterectomy_or_death_only]
    @pph_episodes = @pph_episodes.find_all {|e| e.any_blood_products? } if @options[:with_blood_products_only]

    if current_user.maternity_unit.id == 999
      @monthly_stats_updates = MaternityUnit.all.map {|msu| msu.implied_monthly_stats_updates}.flatten
    else
      @monthly_stats_updates = current_user.maternity_unit.implied_monthly_stats_updates
    end

    if request.format == 'html'
      @pph_episodes = Kaminari.paginate_array(@pph_episodes).page(params[:page]).per(50)
    end

  end

  def destroy
    if params['pph_episode']['local_identifier_for_destroy'] == @pph_episode.local_identifier
      flash[:notice] = "Episode for #{@pph_episode.local_identifier} deleted"

      # This is necessary because the default eager-loading results in ActiveRecord marking things as read-only
      @pph_episode.destroy

      redirect_to pph_episodes_url
    else
      flash[:warning] = "The local identifier that you entered did not match this episode's."
      render :edit
    end
  end

  def create

    transactionally_save_episode_with_results

    if @pph_episode.any_errors?
      render :new
    else
      redirect_to pph_episodes_url
    end
  end

  def edit
    #load_and_authorize_resource magic!
  end

  def show
    redirect_to edit_pph_episode_url(@pph_episode)
  end

  def update

    transactionally_save_episode_with_results

    if @pph_episode.any_errors?
      render :edit
    else
      redirect_to pph_episodes_url
    end
  end

  private

    def transactionally_save_episode_with_results
      ActiveRecord::Base.transaction do
        @pph_episode.update(pph_episode_params)
        
        # Blood Gas Results
        if params['blood_gas_results'] and params['blood_gas_results'].any?
          @pph_episode.blood_gas_results.delete_all

          params['blood_gas_results'].each do |bi|
            bgr = params['blood_gas_results'][bi].permit(BloodGasResult::PERMITTED_PARAMS)
            next if bgr.values.map { |v| v.empty? ? nil : v}.compact.empty?
            @pph_episode.blood_gas_results.build(bgr).save
          end
        end

        # Lab Test Results
        if params['lab_test_results'] and params['lab_test_results'].any?
          @pph_episode.lab_test_results.delete_all

          params['lab_test_results'].each do |li|
            ltr = params['lab_test_results'][li].permit(LabTestResult::PERMITTED_PARAMS)
            next if ltr.values.map { |v| v.empty? ? nil : v}.compact.empty?
            @pph_episode.lab_test_results.build(ltr).save
          end
        end

        # Blood Results

        if params['blood_results'] and params['blood_results'].any?
          @pph_episode.blood_results.delete_all

          params['blood_results'].each do |li|
            br = params['blood_results'][li].permit(BloodResult::PERMITTED_PARAMS)
            next if br.values.map { |v| v.empty? ? nil : v}.compact.empty?
            @pph_episode.blood_results.build(br).save
          end
        end

        # ROTEM Results
        @pph_episode.rotem_results.delete_all

        params['rotem_results'].each do |ri|
          rtr = params['rotem_results'][ri].permit(ROTEMResult::PERMITTED_PARAMS)
          next if rtr.values.map { |v| v.empty? ? nil : v}.compact.empty?
          @pph_episode.rotem_results.build(rtr).save
        end

      end
    end

    def pph_episode_params
      params.require(:pph_episode).permit(
        :local_identifier,
        :maternity_unit_id,
        :delivered_at, :discharged_at, 
        :number_of_deliveries, :type_of_delivery, :location_of_delivery,
        :any_deliveries_ended_in_neonatal_death, 
        :woman_received_level_2_hdu_care, :hours_of_level_2_hdu_care,
        :woman_received_level_2_hdu_care_outside_of_delivery_unit,
        :woman_received_level_3_icu_care, :hours_of_level_3_icu_care,
        :woman_underwent_hysterectomy, :woman_died,
        :blood_loss_in_millilitres, :blood_loss_estimated_or_measured,
        :stage_2_activated_or_litre_lost_at,
        :cause_of_bleed_uterine_atony,
        :cause_of_bleed_surgical,
        :cause_of_bleed_genital_tract_trauma,
        :cause_of_bleed_uterine_rupture,
        :cause_of_bleed_uterine_inversion,
        :cause_of_bleed_abruption,
        :cause_of_bleed_placenta_praevia,
        :cause_of_bleed_retained_products,
        :cause_of_bleed_placenta_ac_increta,
        :cause_of_bleed_amniotic_fluid_embolism,
        :cause_of_bleed_extragenital_bleed,
        :cause_of_bleed_no_cause_trigger_not_reached,
        :risk_assessment_completed,
        :four_stage_paperwork_used,
        :cell_salvage_used,
        :cell_salvage_blood_returned_in_millilitres,
        :haemostatic_uterine_suture_performed,
        :bakri_balloon_performed,
        :internal_iliac_artery_ligation_performed,
        :interventional_radiology_involved,
        :fresh_frozen_plasma_used_in_units, 
        :platelets_used_in_units,
        :fibrinogen_used_in_grams,
        :cryoprecipitate_used_in_pools,
        :red_blood_cell_transfused_in_units,
        :recombinant_factor_viia_used_in_micrograms,
        :blood_products_were_given_according_to_rotem_protocol,
        :confirm_no_rotems
        )
    end

end
