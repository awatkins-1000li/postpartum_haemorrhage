class BulletinsController < ApplicationController

  check_authorization
  load_and_authorize_resource except: [:new]

  def index

    if current_user.maternity_unit.id == 999 then
      @bulletins = Bulletin.order(post_at: :desc).all
    else
      @bulletins = Bulletin.where('post_at <= ?', DateTime.now).order(post_at: :desc).all
    end

    @bulletins.each {|b| authorize! :read, b}

  end

  def show
  end

  def edit
  end

  def new
    @bulletin = Bulletin.new(post_at: (DateTime.now + 1.day).beginning_of_day, title:'Title of your bulletin', markdown_text:'This is a *new* bulletin.')
    authorize! :create, @bulletin
  end

  def create
    @bulletin = Bulletin.new(bulletin_params)
    authorize! :create, @bulletin 
    @bulletin.save
    redirect_to @bulletin
  end

  def destroy
    @bulletin.delete
    redirect_to bulletins_url
  end

  def update
    @bulletin.update(bulletin_params)
    authorize! :update, @bulletin 
    @bulletin.save
    redirect_to @bulletin
  end

  def bulletin_params
    params.require(:bulletin).permit(:title, :post_at, :markdown_text)
  end  

end
