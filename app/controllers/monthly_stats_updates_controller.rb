class MonthlyStatsUpdatesController < ApplicationController

  check_authorization
  load_and_authorize_resource except:[:index, :by_unit]

  skip_authorization_check :only => :by_unit

  def new
    #load_and_authorize_resource magic!
    @monthly_stats_update.month = Date.parse(params['month']) if Date.parse(params['month'])
  end

  def index
    @monthly_stats_updates = current_user.maternity_unit.implied_monthly_stats_updates

    if params['max_months'] and params['max_months'].to_i > 0 and params['max_months'].to_i < @monthly_stats_updates.count
      @monthly_stats_updates = @monthly_stats_updates[0..(params['max_months'].to_i-1)]
    end

    @monthly_stats_updates.each {|msu| authorize! :read, msu }

    respond_to do |format|
      format.html
      format.xlsx
      format.json{
        render :json => @monthly_stats_updates.to_json
      }
    end

  end

  def edit
    #load_and_authorize_resource magic!
  end

  def show
    redirect_to edit_monthly_stats_updates_url(@monthly_stats_update)
  end

  def create

    @monthly_stats_update.save

    if @monthly_stats_update.errors.any?
      render :new
    else
      redirect_to monthly_stats_updates_url
    end
  end

  def update

    @monthly_stats_update.update(monthly_stats_update_params)
    @monthly_stats_update.save

    if @monthly_stats_update.errors.any?
      render :edit
    else
      redirect_to monthly_stats_updates_url
    end
  end

  def by_unit

    excluded_unit_ids = [1, 999]

    endDate = 4.months.ago 
    startDate = endDate - 9.months

    startDate = Date.parse(params['start'].to_s) if params['start']
    endDate = Date.parse(params['end'].to_s) if params['end']

    # Leave a 3-4 month gap for data entry
    if endDate > 4.months.ago
      endDate = 4.months.ago
    end
    endDate = endDate.beginning_of_month.to_date

    # Minimum of 6 months
    if startDate > (endDate - 5.months)
      startDate = (endDate - 5.months)
    end
    startDate = startDate.beginning_of_month.to_date


    # Don't allow it to go back further than January 2017 though!
    if startDate < Date.new(2017,1,1)
      startDate = Date.new(2017,1,1)

      # And then reinforce the 6 months rule
      if startDate > (endDate - 5.months)
        endDate = startDate + 5.months
      end
    end

    period = startDate..endDate

    rethash = {
      total_maternities:{},
      pphs_gte_1000ml:{},
      pphs_gte_1500ml:{},
      pphs_gte_2000ml:{},
      pphs_gte_2500ml:{},
      pphs_gt_1000ml:{},
      pphs_gt_1500ml:{},
      pphs_gt_2000ml:{},
      pphs_gt_2500ml:{},
      episodes_gte_2rbc:{},
      episodes_gte_5rbc:{}
    }

    MaternityUnit.all.each do |unit|
      next if excluded_unit_ids.include? unit.id

      unit.implied_monthly_stats_updates.each do |msu|
        next if not period.include? msu.month

        msu_as_hash = msu.as_json

        rethash.keys.each do |k|
          rethash[k][unit.name] = 0 if not rethash[k][unit.name]
          rethash[k][unit.name] += msu_as_hash[k]
        end

      end
    end

    @start_date = startDate
    @end_date = endDate
    @data = rethash

    rethash = {thisUnit: current_user.maternity_unit.name, startDate: startDate, endDate: endDate, data: rethash }

    respond_to do |format|
      format.xlsx
      format.json{
        render :json => rethash.to_json
      }
    end

  end

  private

  def monthly_stats_update_params
      params.require(:monthly_stats_update).permit(
        :month,
        :obstetric_led_maternities,
        :obstetric_led_deliveries,
        :alongside_midwifery_led_maternities,
        :alongside_midwifery_led_deliveries,
        :freestanding_midwifery_led_maternities,
        :freestanding_midwifery_led_deliveries,
        :home_maternities,
        :home_deliveries
      )
  end

end
