module ParsleyHelper

  def parsley_properties_for_field(model, field)
    properties = {}

    validators = model.validators_on(field)

    # Numbers
    numericality_validator = validators.find {|v| v.class == ActiveModel::Validations::NumericalityValidator}
    if numericality_validator      
      properties['parsley-type'] = 'integer' if numericality_validator.options[:only_integer]

      properties['parsley-min'] = numericality_validator.options[:greater_than_or_equal_to] if numericality_validator.options[:greater_than_or_equal_to]
      properties['parsley-max'] = numericality_validator.options[:less_than_or_equal_to] if numericality_validator.options[:less_than_or_equal_to]
    end

    properties['parsley-validation-threshold'] = '0'
    properties['parsley-trigger'] = 'keyup focusout'
    properties['parsley-debounce'] = '400'

    return properties
  end
end
