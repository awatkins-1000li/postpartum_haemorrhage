Rails.application.routes.draw do
  devise_for :users

  root 'home#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get '/charts', to: 'home#charts'
  get '/funnel', to: 'home#funnel'

  resources :pph_episodes do
    collection do
      get 'new_or_find'
    end
  end

  resources :monthly_stats_updates do
    collection do
      get 'by_unit'
    end
  end

  resources :bulletins

end
