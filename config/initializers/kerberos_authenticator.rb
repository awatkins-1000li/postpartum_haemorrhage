KerberosAuthenticator.setup do |config|
  # Information for the Kerberos 5 library passed through environmental variables is ignored by default.
  # (See http://web.mit.edu/kerberos/krb5-current/doc/admins/env_variables.html)
  # If you want to use these environmental variables, uncomment the line below.
  # (This has no effect if you're using the Heimdal library.)
  config.krb5.use_secure_context = false

  # The authenticator requests ticket-granting-tickets (TGTs) by default.
  # You can request tickets for a specific service by editing the line below.
  config.service = ENV['KA_SERVER']

  # Configure the server principal and keytab used to verify the credentials received from the KDC.
  # Setting these to nil will let the underlying Kerberos 5 library try its own defaults.
  config.server = ENV['KA_SERVER']
  #config.keytab_path = Figaro.env.ka_keytab

  # Provide a keytab as a Base64 encoded string (e.g from an enviromental variable).
  # This will override keytab_path.
  config.keytab_base64 = ENV['KA_KEYTAB']
end