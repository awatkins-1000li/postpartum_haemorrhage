# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end

ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.acronym 'PPH'     # Postpartum haemorrhage
  inflect.acronym 'PPHs'


  inflect.acronym 'ROTEM'
  inflect.acronym 'ROTEMs'
  inflect.acronym 'FIBTEM'
  inflect.acronym 'EXTEM'
  inflect.acronym 'INTEM'

  inflect.acronym 'rFVIIa'  # Recombinant factor VIIa
  inflect.acronym   'VIIa'  


  inflect.acronym 'aPTT'    # Activated partial thromoplastin time
  inflect.acronym 'PT'      # Prothrombin time

  inflect.acronym 'HDU'     # High-Dependency unit
  inflect.acronym 'ICU'     # Intensive Care Unit
  inflect.acronym 'ITU'     # Intensive Therapy Unit

  # ROTEM results
  inflect.acronym 'A5'      # Clotting thickness after _5_ minutes
  inflect.acronym 'CT'      # Clotting time

end
